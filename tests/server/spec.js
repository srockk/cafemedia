var request = require('supertest');

describe('GET /api/posts', function() {
  var app;

  beforeEach(function() {
    app = require('../../server/main.js');
  });

  it('respond with json', function(done) {
    request(app)
      .get('/api/posts')
      .expect('Content-Type', /json/)
      .expect(200, done);
  });
});

describe('GET /api/posts/top', function() {
  var app;

  beforeEach(function() {
    app = require('../../server/main.js');
  });

  it('respond with csv', function(done) {
    request(app)
      .get('/api/posts/top')
      .expect('Content-Type', /text\/csv/)
      .expect(200, done);
  });
});
