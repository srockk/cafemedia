var path = require('path');

module.exports = function (config) {

  var filesCollection = [
    'node_modules/angular/angular.js',
    'node_modules/angular-ui-router/release/angular-ui-router.js',
    'node_modules/angular-date-picker/angular-date-picker.js',
    'public/js/main.js',
    'node_modules/angular-mocks/angular-mocks.js',
    'tests/client/*.js',
    './spec.js'
  ];

  var excludeFiles = [
    'tests/client/karma.conf.js'
  ];

  var configObj = {
    browsers: ['Chrome'],
    frameworks: ['jasmine'],
    basePath: path.join(__dirname, '../../'),
    files: filesCollection,
    exclude: excludeFiles,
    reporters: ['mocha', 'coverage'],
    preprocessors: {},
    coverageReporter: {
      dir: 'coverage/client/',
      reporters: [{
        type: 'text',
        subdir: '.'
      }, {
        type: 'html',
        subdir: '.'
      }]
    }
  };

  config.set(configObj);

};
