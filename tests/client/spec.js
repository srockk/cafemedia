describe('Users factory', function() {
  var posts, $httpBackend, $q, $rootScope;
  var topDaily = [{
    "id": "4840427",
    "title": "When Will You Start to Feel the Baby Move?",
    "privacy": "private",
    "likes": "178",
    "views": "10322",
    "comments": "48",
    "timestamp": "Fri Oct 04 16:29:19 2015"
  }];

  beforeEach(angular.mock.module('Cafemedia'));

  beforeEach(inject(function(_posts_,) {
    posts = _posts_;
  }));

  it('should exist', function() {
    expect(posts).toBeDefined();
  });

  describe('.all()', function() {
    it('should exist', function() {
      expect(posts.all).toBeDefined();
    });
  });

  describe('.daily()', function() {
    it('should return daily top post', function() {
      posts.daily(1443990559000).then(function(data) {
        expect(data).toEqual(topDaily);
      });
    });
  });
});
