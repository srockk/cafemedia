# README #

To run `npm start`

To test backend `npm test`

To test client `karma start tests/client/karma.conf.js`

Asset preprocessor such as Gulp and WebPack are not included.

Screenshot

![Screen Shot 2016-11-04 at 3.51.45 PM.png](https://bitbucket.org/repo/jpe9Mx/images/496164812-Screen%20Shot%202016-11-04%20at%203.51.45%20PM.png)