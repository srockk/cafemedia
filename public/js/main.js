'use strict';

window.app = angular.module('Cafemedia', ['ui.router', 'mp.datePicker']);

app.config(function ($urlRouterProvider, $locationProvider) {
  $locationProvider.html5Mode(true);
  $urlRouterProvider.otherwise('/');
});

app.run(function ($rootScope) {
  $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, thrownError) {
    console.info('The following error was thrown by ui-router while transitioning to state "' + toState.name + '". The origin of this error is probably a resolve function:');
    console.error(thrownError);
  });
});

app.config(function ($stateProvider) {
  $stateProvider.state('home', {
    url: '/',
    templateUrl: 'js/home/home.html',
    controller: 'HomeController',
    resolve: {
      allPosts: ['posts', function(posts) {
        return posts.all().catch(function(error) {
          throw new Error("Unable to retrieve posts ", error);
        });
      }]
    }
  });
});

app.controller('HomeController', function ($scope, allPosts, posts) {
  $scope.posts = allPosts;
  
  $scope.downloadCsv = function(csv) {
    var path;
    if (csv === 'daily') {
      var date = new Date($scope.daily);
      path = csv + '/' + date.getTime();
    } else {
      path = csv;
    }
    window.location = '/api/posts/' + path;
  };

  $scope.filterPosts = function(query) {
    if (query === 'top') {
      posts.top().then(function(posts) {
        $scope.posts = posts;
      });
    } else if (query === 'other') {
      posts.other().then(function(posts) {
        $scope.posts = posts;
      });
    } else if (query === 'daily') {
      posts.daily($scope.daily).then(function(posts) {
        $scope.posts = posts;
      });
    } else {
      posts.all().then(function(posts) {
        $scope.posts = posts;
      });
    }
  };

  $scope.daily = new Date();
});

app.factory('posts', function ($http) {
  var isTopPost = function(post) {
    return post.privacy === 'public' &&
       post.views > 9000 &&
       post.comments > 10 &&
       post.title.length < 40;
  };

  return {
    all: function() {
      return $http.
        get('api/posts').
        then(function(result) {
          return result.data;
        });
    },
    top: function() {
      return $http.
        get('api/posts').
        then(function(result) {
          return result.data.filter(isTopPost);
        });
    },
    other: function() {
      return $http.
        get('api/posts').
        then(function(result) {
          return result.data.filter(function(post) {
            return !isTopPost(post);
          });
        });
    },
    daily: function(day) {
      return $http.
        get('api/posts').
        then(function(result) {
          var postsForToday = result.data.filter(function(post) {
            var postTime = new Date(post.timestamp);
            var now = new Date(day) || new Date();

            return postTime.toDateString() === now.toDateString();
          });

          if (postsForToday.length === 0) {
            return [];
          } else if (postsForToday.length === 1) {
            return postsForToday;
          } else {
            return postsForToday.sort(function(a, b) {
              return b.likes - a.likes;
            }).slice(0, 1);
          }
        });
    }
  };
});
