var fs = require('fs');
var csvjson = require('csvjson');

var csv = fs.readFileSync(__dirname + '/posts.csv', { encoding : 'utf8'});
var data = csvjson.toObject(csv);

var isTopPost = function(post) {
  return post.privacy === 'public' &&
         post.views > 9000 &&
         post.comments > 10 &&
         post.title.length < 40;
};

module.exports = {
  all: function() {
    return data;
  },
  top: function() {
    return data.filter(isTopPost);
  },
  other: function() {
    return data.filter(function(post) {
      return !isTopPost(post);
    });
  },
  daily: function(day) {
    var postsForToday = data.filter(function(post) {
        
      var postTime = new Date(post.timestamp);
      var now = new Date(Number(day)) || new Date();

      return postTime.toDateString() === now.toDateString();
    });

    if (postsForToday.length === 0) {
      return [];
    } else if (postsForToday.length === 1) {
      return postsForToday;
    } else {
      return postsForToday.sort(function(a, b) {
        return b.likes - a.likes;
      }).slice(0, 1);
    }
  }
};
