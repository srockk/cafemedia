var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var app = express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '/../node_modules')));
app.use(express.static(path.join(__dirname, '/../public')));
app.use(express.static(path.join(__dirname, '/../client')));

app.use('/api', require('./routes'));

app.use(function (req, res, next) {
  var err;
  if (path.extname(req.path).length > 0) {
    err = new Error('Not found.');
    err.status = 404;
    next(err);
  } else {
    next();
  }
});

app.get('/*', function(req, res) {
  res.sendFile(path.join(__dirname, '/views/index.html'));
});

app.use(function (err, req, res, next) {
    console.error(err);
    console.error(err.stack);
    res.status(err.status || 500).send(err.message || 'Internal server error.');
});

module.exports = app;
