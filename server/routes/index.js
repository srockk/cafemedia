'use strict';
var router = require('express').Router();

router.use('/posts', require('./posts'));

router.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

module.exports = router;
