var express = require('express');
var router = express.Router();
var csvjson = require('csvjson');
var posts = require('../../models/posts');

router.get('/', function(req, res) {
  res.json(posts.all());
});

router.get('/top', function(req, res) {
  var data = posts.top();
  var csv = csvjson.toCSV(data, {headers: 'key'});
  res.setHeader('Content-disposition', 'attachment; filename=top.csv');
  res.set('Content-Type', 'text/csv');
  res.status(200).send(csv);
});

router.get('/other', function(req, res) {
  var data = posts.other();
  var csv = csvjson.toCSV(data, {headers: 'key'});
  res.setHeader('Content-disposition', 'attachment; filename=other.csv');
  res.set('Content-Type', 'text/csv');
  res.status(200).send(csv);
});

router.get('/daily/:day', function(req, res) {
  var data = posts.daily(req.params.day);
  var csv = csvjson.toCSV(data, {headers: 'key'});
  res.setHeader('Content-disposition', 'attachment; filename=daily.csv');
  res.set('Content-Type', 'text/csv');
  res.status(200).send(csv);
});

module.exports = router;
