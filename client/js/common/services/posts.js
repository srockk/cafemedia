app.factory('posts', function ($http) {
  var isTopPost = function(post) {
    return post.privacy === 'public' &&
      post.views > 9000 &&
      post.comments > 10 &&
      post.title.length < 40;
  };

  return {
    all: function() {
      return $http.
        get('api/posts').
        then(function(result) {
          return result.data;
        });
    },
    top: function() {
      return $http.
        get('api/posts').
        then(function(result) {
          return result.data.filter(isTopPost);
        });
    },
    other: function() {
      return $http.
        get('api/posts').
        then(function(result) {
          return result.data.filter(function(post) {
            return !isTopPost(post);
          });
        });
    },
    daily: function(day) {
      return $http.
        get('api/posts').
        then(function(result) {
          var postsForToday = result.data.filter(function(post) {
            var postTime = new Date(post.timestamp);
            var now = new Date(day) || new Date();

            return postTime.toDateString() === now.toDateString();
          });

          if (postsForToday.length === 0) {
            return [];
          } else if (postsForToday.length === 1) {
            return postsForToday;
          } else {
            return postsForToday.sort(function(a, b) {
              return b.likes - a.likes;
            }).slice(0, 1);
          }
        });
    }
  };
});
