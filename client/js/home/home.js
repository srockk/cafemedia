app.config(function ($stateProvider) {
  $stateProvider.state('home', {
    url: '/',
    templateUrl: 'js/home/home.html',
    controller: 'HomeController',
    resolve: {
      allPosts: ['posts', function(posts) {
        return posts.all().catch(function(error) {
          throw new Error("Unable to retrieve posts ", error);
        });
      }]
    }
  });
});

app.controller('HomeController', function ($scope, allPosts, posts) {
  $scope.posts = allPosts;
  
  $scope.downloadCsv = function(csv) {
    var path;
    if (csv === 'daily') {
      var date = new Date($scope.daily);
      path = csv + '/' + date.getTime();
    } else {
      path = csv;
    }
    window.location = '/api/posts/' + path;
  };

  $scope.filterPosts = function(query) {
    if (query === 'top') {
      posts.top().then(function(posts) {
        $scope.posts = posts;
      });
    } else if (query === 'other') {
      posts.other().then(function(posts) {
        $scope.posts = posts;
      });
    } else if (query === 'daily') {
      posts.daily($scope.daily).then(function(posts) {
        $scope.posts = posts;
      });
    } else {
      posts.all().then(function(posts) {
        $scope.posts = posts;
      });
    }
  };

  $scope.daily = new Date();
});
